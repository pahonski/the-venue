import React from 'react';

const Location = () => {
  return (
    <div className='location_wrapper'>
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4703.4454146160315!2d27.590278507598875!3d53.88335750884898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dbcfeed1ee2a89%3A0xfbfb4ec7a1a62153!2z0YPQu9C40YbQsCDQndC10LzQuNCz0LAsINCc0LjQvdGB0Lo!5e0!3m2!1sru!2sby!4v1559493108369!5m2!1sru!2sby"
        width="100%"
        height="500px"
        frameBorder="0"
        allowFullScreen>
      </iframe>

      <div className='location_tag'>
        <div>
          Location
        </div>
      </div>

    </div>
  );
};

export default Location;