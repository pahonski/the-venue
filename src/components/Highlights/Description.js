import React from 'react';
import Fade from 'react-reveal/Fade';

const Description = () => {
  return (
    <Fade>
      <div className='center_wrapper'>
        <h2>Highlights</h2>
        <div className='highlight_description'>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab, animi atque, corporis culpa deleniti, dolore doloremque facere fugit molestias natus non odit quaerat quisquam sit suscipit tempora veniam vero.
        </div>
      </div>
    </Fade>
  );
};

export default Description;